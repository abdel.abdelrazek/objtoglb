var express = require('express');
var obj2gltf = require('obj2gltf');
  var fs = require('fs');

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  try{

    var options = {
      binary : true
  }
  obj2gltf('./model/test2.obj', options)
      .then(function(glb) {
          fs.writeFileSync('./model/res/model2.glb', glb);

          res.json({"status":1});

      });
    }
    catch(err){
      res.json({"status":0})
    }
});

module.exports = router;
